# LDAP Server
## @edt ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona

 * **isx41016667/ldap21:versio1** Servidor LDAP base inicial amb la base de dades edt.org
```
# Engegar la maquina LDAP amb detach:
docker run --rm --name ldap.edt.org -h ldap.edt.org --net hisx2 -d isx41016667/ldap21:versio1

# Des d'una altre consola, fem les consultes LDAP:
docker exec -it ldap.edt.org /bin/bash
ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'
o
ldapsearch -x -LLL -h 172.17.0.2 -b 'dc=edt,dc=org'
```
 * **Dockerfile** Fitxer d'instal·lacio de la maquina LDAP.
 * **startup.sh** Instruccions d'inici de sessio.
 * **slapd.conf** Fitxer de configuracio del servidor LDAP.
 * **organitzacio-edt.org.ldif** Base de dades edt.org.
 * **usuaris-edt.org.ldif** Usuaris de la base de dades edt.org.
 * **usuaris-mes-edt.org.ldif** Mes usuaris de la base de dades edt.org.
 * **edt.org.ldif** Fitxer amb tots els usuaris(usuaris-edt.org,usuaris-mes-edt.org) de la base de dades.
 * **modify[1-2].ldif** Fitxers amb modificacios de dades dels usuaris de la base de dades, exemples per l'ordre "ldapmodify".
 * **eliminats.ldif** Fitxer amb usuaris a eliminar, exemples per l'ordre "ldapdelete".
 * **afegir.ldif** Fitxer amb usuaris a afegir, exemples per l'ordre "ldapadd".
