# LDAP Server
## @edt ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **isx41016667/ldap21:base** Servidor LDAP base inicial amb la base de dades edt.org
```
docker network create hisx2
docker build -t edtasixm06/ldap21:base .

docker run --rm --name ldap.edt.org -h ldap.edt.org --net hisx2 -it cristiancondolo21/ldap21:base /bin/bash

ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'
```
 * **Dockerfile** Fitxer d'instal·lacio de la maquina LDAP.
 * **startup.sh** Instruccions d'inici de sessio.
 * **slapd.conf** Fitxer de configuracio del servidor LDAP.
 * **organitzacio-edt.org.ldif** Base de dades edt.org.
 * **usuaris-edt.org.ldif** Usuaris de la base de dades edt.org.
 * **usuaris-mes-edt.org.ldif** Mes usuaris de la base de dades edt.org.
