# EXEMPLES/CASOS AMB ACL
## **EXEMPLE 1** Tots poguim veure tot.
#### -- /opt/docker/acl1.ldif
```
# Modificacio de ACLs
# 01-Access de lectura a tothom
#================================
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * read
```
```
root@ldap:/opt/docker# ldapmodify -xv -D "cn=Sysadmin,cn=config" -w syskey -f acl1.ldif
ldap_initialize( <DEFAULT> )
delete olcAccess:
add olcAccess:
	to * by * read
modifying entry "olcDatabase={1}mdb,cn=config"
modify complete

```
* 	**Cas1** Veure tot com *anonymous*.
```
	root@ldap:/opt/docker# ldapwhoami -x
	anonymous
	dn: dc=edt,dc=org
	dc: edt
	description: Escola del treball de Barcelona
	objectClass: dcObject
	objectClass: organization
	o: edt.org
	
	dn: ou=maquines,dc=edt,dc=org
	ou: maquines
	description: Container per a maquines linux
	objectClass: organizationalunit
	...
	
	dn: uid=new02,ou=usuaris,dc=edt,dc=org
	objectClass: posixAccount
	objectClass: inetOrgPerson
	cn: new02
	sn: new02
	uid: new02
	uidNumber: 7021
	gidNumber: 614
	homeDirectory: /tmp/home/2hiaw/new02
	userPassword:: e1NTSEF9YWVVTzR2MCtZb0dadDY1bkovZDBXb1ZTNVg0MnZjZ3U=
```
* 	**Cas2** Accedir com un usuari (per exemple: l'Anna) i veure tant el seu con els altres.
```
	root@ldap:/opt/docker# ldapsearch -x -LLL -D 'uid=anna,ou=usuaris,dc=edt,dc=org' -w anna uid=anna
	dn: uid=anna,ou=usuaris,dc=edt,dc=org
	objectClass: posixAccount
	objectClass: inetOrgPerson
	cn: Anna Pou
	cn: Anita Pou
	sn: Pou
	homePhone: 555-222-2222
	mail: anna@edt.org
	description: Watch out for this girl
	ou: Alumnes
	uid: anna
	uidNumber: 5002
	gidNumber: 600
	homeDirectory: /tmp/home/anna
	userPassword:: e1NTSEF9Qm00QjNCdS9mdUg2QmJ5OWxneGZGQXdMWXJLMFJiT3E=

	root@ldap:/opt/docker# ldapsearch -x -LLL -D 'uid=anna,ou=usuaris,dc=edt,dc=org' -w anna uid=pere
	dn: uid=pere,ou=usuaris,dc=edt,dc=org
	objectClass: posixAccount
	objectClass: inetOrgPerson
	cn: Pere Pou
	sn: Pou
	homePhone: 555-222-2221
	mail: pere@edt.org
	description: Watch out for this guy
	ou: Profes
	uid: pere
	uidNumber: 5001
	gidNumber: 100
	homeDirectory: /tmp/home/pere
	userPassword:: e1NTSEF9Z2htdFJMMTFZdFhvVWhJUDd6NmY3bmI4UkNOYWRGZSs=
```

*	 **Cas3** Provar modificar les teves propies dades.
####	**Utilitzare el mateix fitxer per fer les modificacions de dades en tots els exemples**
####	-- /opt/docker/fitxer-modify.ldif
```
	# Modificacion de dades
	# usuari (pere) modifiqui les seves propies dades
	#=================================================
	dn: uid=pere,ou=usuaris,dc=edt,dc=org
	changetype: modify
	replace: cn
	cn: Pere Montserrat
```
```	
	root@ldap:/opt/docker# ldapmodify -xv -D "uid=pere,ou=usuaris,dc=edt,dc=org" -w pere -f fitxer-modify.ldif 
	ldap_initialize( <DEFAULT> )
	add homePhone:
		555-222-2021
	modifying entry "uid=pere,ou=usuaris,dc=edt,dc=org"
	ldap_modify: Insufficient access (50)
```
*	**Cas4** Provar modificar les dades dels atres usuaris.
####	-- /opt/docker/fitxer-modify.ldif
```
	# Modificacion de dades
	# usuari (pere) modifiqui les dades d'un altre usuari
	#=====================================================
	dn: uid=pau,ou=usuaris,dc=edt,dc=org
	changetype: modify
	add: mail
	mail: paupau@edt.org
```
```
	root@ldap:/opt/docker# ldapmodify -xv -D "uid=pere,ou=usuaris,dc=edt,dc=org" -w pere -f fitxer-modify.ldif 
	ldap_initialize( <DEFAULT> )
	add mail:
		paupau@edt.org
	modifying entry "uid=pau,ou=usuaris,dc=edt,dc=org"
	ldap_modify: Insufficient access (50)
```

## **EXEMPLE 2** Tots poguim modificar-hi tot.
#### -- /opt/docker/acl2.ldif
```
# Modificacio de ACLs
# 02-Access de escriptura a tothom
#===================================
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * write
```
```
root@ldap:/opt/docker# ldapmodify -xv -D "cn=Sysadmin,cn=config" -w syskey -f acl2.ldif 
delete olcAccess:
add olcAccess:
	to * by * write
modifying entry "olcDatabase={1}mdb,cn=config"
modify complete

```
*	**Cas1** Que modifiqui les meves dades
####	-- /opt/docker/fitxer-modify.ldif
```
	# Modificacion de dades
	# usuari (pere) modifiqui les seves dades
	#=====================================================
	dn: uid=pere,ou=usuaris,dc=edt,dc=org
	changetype: modify
	add: homePhone
	homePhone: 555-222-2021
```
```
	root@ldap:/opt/docker# ldapmodify -xv -D "uid=pere,ou=usuaris,dc=edt,dc=org" -w pere -f fitxer-modify.ldif 
	ldap_initialize( <DEFAULT> )
	add homePhone:
		555-222-2021
	modifying entry "uid=pere,ou=usuaris,dc=edt,dc=org"
	modify complete

	root@ldap:/opt/docker# ldapsearch -x -LLL uid=pere
	dn: uid=pere,ou=usuaris,dc=edt,dc=org
	objectClass: posixAccount
	objectClass: inetOrgPerson
	cn: Pere Pou
	sn: Pou
	mail: pere@edt.org
	description: Watch out for this guy
	ou: Profes
	uid: pere
	uidNumber: 5001
	gidNumber: 100
	homeDirectory: /tmp/home/pere
	userPassword:: e1NTSEF9Z2htdFJMMTFZdFhvVWhJUDd6NmY3bmI4UkNOYWRGZSs=
	homePhone: 555-222-2221
	homePhone: 555-222-2021
```
*	**Cas2** Que modifiqui les dades d'un altre usuari
####	-- /opt/docker/fitxer-modify.ldif
```
	# Modificacion de dades
	# usuari (pere) modifiqui les dades d'un altre usuari
	#=====================================================
	dn: uid=pau,ou=usuaris,dc=edt,dc=org
	changetype: modify
	add: mail
	mail: paupau@edt.org
```
```
	root@ldap:/opt/docker# ldapmodify -xv -D "uid=pere,ou=usuaris,dc=edt,dc=org" -w pere -f fitxer-modify.ldif 
	ldap_initialize( <DEFAULT> )
	add mail:
		paupau@edt.org
	modifying entry "uid=pau,ou=usuaris,dc=edt,dc=org"
	modify complete

	root@ldap:/opt/docker# ldapsearch -x -LLL uid=pau 
	dn: uid=pau,ou=usuaris,dc=edt,dc=org
	objectClass: posixAccount
	objectClass: inetOrgPerson
	cn: Pau Pou
	cn: Pauet Pou
	sn: Pou
	homePhone: 555-222-2220
	mail: pau@edt.org
	mail: paupau@edt.org
	description: Watch out for this guy
	ou: Profes
	uid: pau
	uidNumber: 5000
	gidNumber: 100
	homeDirectory: /tmp/home/pau
	userPassword:: e1NTSEF9TkRraXBlc05RcVRGRGdHSmZ5cmFMei9jc1pBSWxrMi8=
```

## **EXEMPLE 3** Acces de lectura a tothom. Acces d'escriptura a usuaris a les seves propies dades.
#### -- /opt/docker/acl3.ldif
```
# Modificacio de ACLs
# 03-Access de lectura a tothom
# Acces de escriptura als usuaris a les seves dades
#====================================================
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
-
add: olcAccess
olcAccess: to by self write by * read
```
```
root@ldap:/opt/docker# ldapmodify -xv -D "cn=Sysadmin,cn=config" -w syskey -f acl3.ldif 
ldap_initialize( <DEFAULT> )
delete olcAccess:
add olcAccess:
	to by self write by * read
modifying entry "olcDatabase={1}mdb,cn=config"
modify complete
```	
*	**Cas1** Que modifiqui les meves dades
####	-- /opt/docker/fitxer-modify.ldif
```
	# Modificacion de dades
	# usuari (pere) modifiqui les seves propies dades
	#=====================================================
	dn: uid=pere,ou=usuaris,dc=edt,dc=org
	changetype: modify
	add: cn
	cn: Peter Parker
```
```
	root@ldap:/opt/docker# ldapmodify -xv -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -f fitxer-modify.ldif 
	ldap_initialize( <DEFAULT> )
	add cn:
		Peter Parker
	modifying entry "uid=pere,ou=usuaris,dc=edt,dc=org"
	modify complete

	root@ldap:/opt/docker# ldapsearch -x -LLL uid=pere
	dn: uid=pere,ou=usuaris,dc=edt,dc=org
	objectClass: posixAccount
	objectClass: inetOrgPerson
	cn: Pere Pou
	cn: Peter Parker
	sn: Pou
	homePhone: 555-222-2221
	mail: pere@edt.org
	description: Watch out for this guy
	ou: Profes
	uid: pere
	uidNumber: 5001
	gidNumber: 100
	homeDirectory: /tmp/home/pere
	userPassword:: e1NTSEF9Z2htdFJMMTFZdFhvVWhJUDd6NmY3bmI4UkNOYWRGZSs=
```
*	**Cas2** Que modifiqui les dades d'un altre usuari
####	-- /opt/docker/fitxer-modify.ldif
```
	# Modificacion de dades
	# usuari (pere) modifiqui les dades d'altres usuaris
	#=====================================================
	dn: uid=marta,ou=usuaris,dc=edt,dc=org
	changetype: modify
	add: homePhone
	homePhone: 111-222-333
```
```
	root@ldap:/opt/docker# ldapmodify -xv -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -f fitxer-modify.ldif 
	ldap_initialize( <DEFAULT> )
	add homePhone:
		111-222-333
	modifying entry "uid=marta,ou=usuaris,dc=edt,dc=org"
	ldap_modify: Insufficient access (50)
```
*	**Cas3** Veure les dades de altres i/o la base de dades
```
	root@ldap:/opt/docker# ldapsearch -x -LLL -D'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere uid=marta 
	dn: uid=marta,ou=usuaris,dc=edt,dc=org
	objectClass: posixAccount
	objectClass: inetOrgPerson
	cn: Marta Mas
	sn: Mas
	homePhone: 555-222-2223
	mail: marta@edt.org
	description: Watch out for this girl
	ou: Alumnes
	uid: marta
	uidNumber: 5003
	gidNumber: 600
	homeDirectory: /tmp/home/marta
	userPassword:: e1NTSEF9OSsxRjJmNXZjVzh6L3RtU3pZTldkbHo1R2JEQ3lvT3c=
```

## **EXEMPLE 4** Acces als usuaris canviar el seu passwd. Acces de lectura a tothom.
#### -- /opt/docker/acl4.ldif
```
# Modificacio de ACLs
# 04-Access de lectura a tothom
#====================================================
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
-
add: olcAccess
olcAccess: to attrs=userPassword by self write by * auth
olcAccess: to * by * read
```
```
root@ldap:/opt/docker# ldapmodify -xv -D "cn=Sysadmin,cn=config" -w syskey -f acl4.ldif 
ldap_initialize( <DEFAULT> )
delete olcAccess:
add olcAccess:
	to attrs=userPassword by self write by * auth
	to * by * read
modifying entry "olcDatabase={1}mdb,cn=config"
modify complete
```	
*	**Cas1** Canviar el propi passwd.
```
	root@ldap:/opt/docker# ldappasswd -xv -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -s pedro
	ldap_initialize( <DEFAULT> )
	Result: Success (0)
```
*	**Cas2** Canviar el passwd d'altres.
```
	root@ldap:/opt/docker# ldappasswd -xv -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere 'uid=jordi,ou=usuaris,dc=edt,dc=org' -s george
	ldap_initialize( <DEFAULT> )
	Result: Insufficient access (50)
```
*	**Cas3** Modificar  altres.
####	-- /opt/docker/fitxer-modify.ldif
```
	# Modificacion de dades
	# usuari (anna) modifiqui les dades d'altres usuaris
	#=====================================================
	dn: uid=new01,ou=usuaris,dc=edt,dc=org
	changetype: modify
	add: mail
	mail: nuevo@edt.org
```
```
	root@ldap:/opt/docker# ldapmodify -xv -D 'uid=marta,ou=usuaris,dc=edt,dc=org' -w marta -f fitxer-modify.ldif 
	ldap_initialize( <DEFAULT> )
	add mail:
		nuevo@edt.org
	modifying entry "uid=new01,ou=usuaris,dc=edt,dc=org"
	ldap_modify: Insufficient access (50)
```
*	**Cas4** Veure tota la base de dades.
```
	root@ldap:/opt/docker# ldapsearch -x -LLL -D 'uid=pau,ou=usuaris,dc=edt,dc=org' -w pau 
	dn: dc=edt,dc=org
	dc: edt
	description: Escola del treball de Barcelona
	objectClass: dcObject
	objectClass: organization
	o: edt.org

	dn: ou=maquines,dc=edt,dc=org
	ou: maquines
	description: Container per a maquines linux
	objectClass: organizationalunit
	...
	dn: uid=new02,ou=usuaris,dc=edt,dc=org
	objectClass: posixAccount
	objectClass: inetOrgPerson
	cn: new02
	sn: new02
	uid: new02
	uidNumber: 7021
	gidNumber: 614
	homeDirectory: /tmp/home/2hiaw/new02
```
