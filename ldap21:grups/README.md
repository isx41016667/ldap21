# LDAP Server
## @edt ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **edtasixm06/ldap21:editat** Servidor LDAP amb la base de dades edt.org. Container per deures.
   S'ha fet el següent:
	* generar un sol fitxer ldif anomenat edt.org.ldif
	* modificar el fitxer edt.org.ldif  modificant dn dels usuaris utilitzant en lloc del cn
	  el uid per identificar-los.
	* configurar el password de Manager que sigui ‘secret’ però encriptat (posar-hi un
	  comentari per indicar quin és de cara a estudiar).
	* afegir el fitxer de configuració client.
	* propagar el port amb -P  -p
			
```
docker network create hisix2
docker build -t cristiancondolo21/ldap21:editat .

docker run --rm --name ldap.edt.org -h ldap.edt.org --net hisx2 -p 389:389 -d edtasixm06/ldap21:editat

docker ps

ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'
``` 


